import { expect, request, use as chai_use } from 'chai'
import chaiHttp = require('chai-http')
import * as HttpStatusCodes from 'http-status-codes'
import app from '../src/api'

chai_use(chaiHttp)

const apiPrefix = '/health'

describe('/health', () => {
  it('get liveliness', async () => {
    const res = await request(app).get(`${apiPrefix}/liveliness`)
    expect(res).to.have.status(HttpStatusCodes.OK)
    expect(res).to.be.json
    expect(res.body.status).to.equal('OK')
  })

  it('get readiness', async () => {
    const res = await request(app).get(`${apiPrefix}/readiness`)
    expect(res).to.have.status(HttpStatusCodes.OK)
    expect(res).to.be.json
    expect(res.body.status).to.equal('OK')
  })
})
