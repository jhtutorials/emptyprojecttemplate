import dotenv from 'dotenv'
dotenv.config({ path: 'service.env' })
import * as http from 'http'
import App from './api'

const port = process.env.PORT
const server = http.createServer(App)
server.listen(port)
server.on('listening', onListening)

function onListening (): void {
  let addr = server.address()
  let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`
  console.info(`Listening on ${bind}`)
}
