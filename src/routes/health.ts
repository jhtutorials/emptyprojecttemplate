import express from 'express'
import { Router } from 'express-serve-static-core'

export function GetRouter (): Router {
  const router = express.Router()

  router.get('/liveliness', (req: express.Request, res: express.Response, next: express.NextFunction) => res.json({ status: 'OK' }))
  router.get('/readiness', (req: express.Request, res: express.Response, next: express.NextFunction) => res.json({ status: 'OK' }))

  return router
}
