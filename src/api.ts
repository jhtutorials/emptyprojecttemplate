import * as bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import * as httpStatusCodes from 'http-status-codes'
import logger from 'morgan'
import { ResponseError } from './errors/response'
import { ServerError } from './errors/server'
import { GetRouter as getHealthRouter } from './routes/health'

const prefix = '/api/v1/stuff/'

class App {
  public express: express.Application

  constructor () {
    this.express = express()
    this.middleware()
    this.routes()
  }

  private middleware (): void {
    this.express.use(logger('[asset-manager] :method :url :status :response-time ms - :res[content-length]'))
    this.express.use(bodyParser.json())
    this.express.use(bodyParser.urlencoded({ extended: false }))
    this.express.use(cors({ optionsSuccessStatus: httpStatusCodes.OK }))
  }

  private routes (): void {
    this.express.use('/health/', getHealthRouter())

    this.express.use(this.errorHandler.bind(this))
  }

  private errorHandler (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
    // bring the error into our format
    // istanbul ignore if
    if (!(err instanceof ResponseError)) {
      err = new ServerError(err.message)
    }
    res.statusCode = err.code
    res.json({ code: err.code, time: (new Date()).toISOString(), message: err.message })
  }
}

const app = new App()
export default app.express
