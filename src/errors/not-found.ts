import HttpStatusCodes from 'http-status-codes'
import { ResponseError } from './response'

export class NotFoundError extends ResponseError {
  constructor (message: string) {
    super(message, HttpStatusCodes.NOT_FOUND)
  }
}
