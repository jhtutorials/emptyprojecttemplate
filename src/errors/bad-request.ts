import HttpStatusCodes from 'http-status-codes'
import { ResponseError } from './response'

export class BadRequestError extends ResponseError {
  constructor (message: string) {
    super(message, HttpStatusCodes.BAD_REQUEST)
  }
}
