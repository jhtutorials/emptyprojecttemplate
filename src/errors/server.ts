import HttpStatusCodes from 'http-status-codes'
import { ResponseError } from './response'

export class ServerError extends ResponseError {
  constructor (message: string) {
    super(message, HttpStatusCodes.INTERNAL_SERVER_ERROR)
  }
}
